const cbAfter = [];
const cbError = [];

let functionWasCalled = false;

function done(arg) {
  if (functionWasCalled === true) {
    return;
  }

  functionWasCalled = true;
  setTimeout(function () {
    let prevRes = null;
    cbAfter.forEach(item => {
      if (!prevRes) {
        prevRes = item(arg);
      } else {
        prevRes = item(prevRes);
      }
      return prevRes;
    })
  })
}

function fail(arg) {
  if (functionWasCalled === true) {
    return;
  }

  functionWasCalled = true;
  setTimeout(function () {
    cbError.forEach(item => item(arg))
  }, 0)
}

class DoAsync {
  constructor(func) {
    func(done, fail)
  }

  after(cb) {
    cbAfter.push(cb);
    return this;
  };

  error(cb) {
    cbError.push(cb);
    return this;
  };
}

const users = [{ name: 'Jack', name: 'Masha' }];

const getUser = new DoAsync(function(done, fail) {
  console.log(1);
  done(users);
  console.log(2);
  fail('Some Error');
  console.log(3);
  done(null);
})

console.log(4);

getUser.after(function(res) {
  console.log(8, res);
});

console.log(5);

getUser.error(function(err) {
  console.log('it has never been called!', err);
});

console.log(6);

getUser.after(function(res) {
  console.log(9, res);
});

console.log(7);

getUser
 .after(function(res) {
  console.log(9.1, res);
  return 10;
 })
 .after(function(res) {
  console.log(9.2, res);
 });

getUser
 .after(function(res) {
  console.log(10, res);
 })
